def parse_ref_file(self, path):
    collection = []

    with codecs.open(path, 'r') as f:
        for l in f.readlines():
            if not self._re_delimiter.search(l):
                continue

            romanji, actual_char = self._re_delimiter.split(l)

            actual_char = actual_char.strip()
            ucode       = ord(actual_char)

            collection.append((ucode, romanji, actual_char))

    return collection
