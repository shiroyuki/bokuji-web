import inspect
import xml.etree.ElementTree as ET

from bokuji.entity import Entry

class JMDictionaryCompiler(object):
    def compile_from_file(self, path):
        tree = ET.parse('dictionary-en.xml')
        root = tree.getroot()

        entry_nodes = root.findall('entry')

        for entry_node in entry_nodes:
            yield self._parse_entry(entry_node)

    def _parse_entry(self, node):
        entry_options = {
            'readings': [
                sub_entry.find('reb').text
                for sub_entry in node.findall('r_ele')
            ],
            'parts_of_speech': [
                p.text
                for p in node.find('sense').findall('pos')
            ],
            'relevant_info': [
                r.text
                for r in node.find('sense').findall('misc')
            ],
            'sequence': int(node.find('ent_seq').text),
            'glossery': {
                (g.get('xml:lang') or 'eng'): g.text
                for g in node.find('sense').findall('gloss')
            },
            'xref': [ # cross reference
                x.text
                for x in node.find('sense').findall('xref')
            ],
            'antonyms': [
                a.text
                for a in node.find('sense').findall('ant')
            ],
            'app_fields': [ # fields of application
                f.text
                for f in node.find('sense').findall('field')
            ],
            'dialects': [
                d.text
                for d in node.find('sense').findall('dial')
            ],
            'kanji': [
                sub_entry.find('keb').text
                for sub_entry in node.findall('k_ele')
            ],
        }

        return Entry(**entry_options)
