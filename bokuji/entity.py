import time

from passerine.db.entity import entity
from passerine.db.mapper import link, AssociationType as t, CascadingType as c
from bokuji.model import Entry as EntryModel

@entity
class Revision(object):
    """ Data Revision """
    def __init__(self, version : int = None):
        self.version = version or time.time()

@link(
    target      = Revision,
    mapped_by   = 'revision',
    association = t.MANY_TO_ONE
)
@entity
class Entry(EntryModel):
    """ Dictionary Entry """
    def __init__(self, revision = None, **kwargs):
        EntryModel.__init__(self, **kwargs)

        self.revision = revision