class Entry(object):
    def __init__(
        self,
        readings        : list,
        parts_of_speech : list,
        relevant_info   : list,
        sequence        : int,
        glossery        : dict,
        xref            : list,
        antonyms        : list,
        app_fields      : list,
        dialects        : list,
        kanji           : list
    ):
        self.readings        = readings
        self.parts_of_speech = parts_of_speech
        self.relevant_info   = relevant_info
        self.sequence        = sequence
        self.glossery        = glossery
        self.xref            = xref
        self.antonyms        = antonyms
        self.app_fields      = app_fields
        self.dialects        = dialects
        self.kanji           = kanji