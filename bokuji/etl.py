import sys
import time
import xml.etree.ElementTree as ET

from gallium.interface import ICommand

from bokuji.entity import Revision, Entry

class Xml2MongoDbTransformer(ICommand):
    """ Stage-1 ETL Process """
    def identifier(self):
        return 'etl.xml_2_db'

    def define(self, parser):
        pass

    def execute(self, args):
        entity_manager = self.core.get('db.default')
        serializer     = self.core.get('passerine.serializer')
        compiler       = self.core.get('compiler.jmdict')
        entries        = [
            entry
            for entry in compiler.compile_from_file('dictionary-en.xml')
        ]

        with entity_manager.session() as session:
            revision = Revision()

            session.persist(revision)
            session.flush()

            processed_count   = 0
            total_entry_count = len(entries)

            for entry in entries:
                entry.revision = revision

                serialized_entry = serializer.encode(entry)
                serialized_entry.update(serializer.extra_associations(entry))

                session.driver.insert('entry', serialized_entry)

                processed_count += 1

                sys.stdout.write('\r{}/{} ({:.2f}%)'.format(
                    processed_count,
                    total_entry_count,
                    processed_count * 100.0 / total_entry_count
                ))

            print('\nFinishing up...')

        print('Done')

    def _serialize(self, entry : Entry):
        serializable = {
            name: getattr(entry, name)
            for name in self._entry_properties
        }
